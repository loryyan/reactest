(window["webpackJsonp"] = window["webpackJsonp"] || []).push([["app"],{

/***/ "./index.js":
/*!******************!*\
  !*** ./index.js ***!
  \******************/
/*! no exports provided */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony import */ var react__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! react */ "../../../node_modules/react/index.js");
/* harmony import */ var react__WEBPACK_IMPORTED_MODULE_0___default = /*#__PURE__*/__webpack_require__.n(react__WEBPACK_IMPORTED_MODULE_0__);
/* harmony import */ var react_dom__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! react-dom */ "../../../node_modules/react-dom/index.js");
/* harmony import */ var react_dom__WEBPACK_IMPORTED_MODULE_1___default = /*#__PURE__*/__webpack_require__.n(react_dom__WEBPACK_IMPORTED_MODULE_1__);
/* harmony import */ var react_trello__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! react-trello */ "../../../node_modules/react-trello/dist/index.js");
/* harmony import */ var react_trello__WEBPACK_IMPORTED_MODULE_2___default = /*#__PURE__*/__webpack_require__.n(react_trello__WEBPACK_IMPORTED_MODULE_2__);
/* harmony import */ var socket_io_client__WEBPACK_IMPORTED_MODULE_3__ = __webpack_require__(/*! socket.io-client */ "../../../node_modules/socket.io-client/lib/index.js");
/* harmony import */ var socket_io_client__WEBPACK_IMPORTED_MODULE_3___default = /*#__PURE__*/__webpack_require__.n(socket_io_client__WEBPACK_IMPORTED_MODULE_3__);
function _typeof(obj) { if (typeof Symbol === "function" && typeof Symbol.iterator === "symbol") { _typeof = function _typeof(obj) { return typeof obj; }; } else { _typeof = function _typeof(obj) { return obj && typeof Symbol === "function" && obj.constructor === Symbol && obj !== Symbol.prototype ? "symbol" : typeof obj; }; } return _typeof(obj); }

function _classCallCheck(instance, Constructor) { if (!(instance instanceof Constructor)) { throw new TypeError("Cannot call a class as a function"); } }

function _defineProperties(target, props) { for (var i = 0; i < props.length; i++) { var descriptor = props[i]; descriptor.enumerable = descriptor.enumerable || false; descriptor.configurable = true; if ("value" in descriptor) descriptor.writable = true; Object.defineProperty(target, descriptor.key, descriptor); } }

function _createClass(Constructor, protoProps, staticProps) { if (protoProps) _defineProperties(Constructor.prototype, protoProps); if (staticProps) _defineProperties(Constructor, staticProps); return Constructor; }

function _possibleConstructorReturn(self, call) { if (call && (_typeof(call) === "object" || typeof call === "function")) { return call; } return _assertThisInitialized(self); }

function _getPrototypeOf(o) { _getPrototypeOf = Object.setPrototypeOf ? Object.getPrototypeOf : function _getPrototypeOf(o) { return o.__proto__ || Object.getPrototypeOf(o); }; return _getPrototypeOf(o); }

function _assertThisInitialized(self) { if (self === void 0) { throw new ReferenceError("this hasn't been initialised - super() hasn't been called"); } return self; }

function _inherits(subClass, superClass) { if (typeof superClass !== "function" && superClass !== null) { throw new TypeError("Super expression must either be null or a function"); } subClass.prototype = Object.create(superClass && superClass.prototype, { constructor: { value: subClass, writable: true, configurable: true } }); if (superClass) _setPrototypeOf(subClass, superClass); }

function _setPrototypeOf(o, p) { _setPrototypeOf = Object.setPrototypeOf || function _setPrototypeOf(o, p) { o.__proto__ = p; return o; }; return _setPrototypeOf(o, p); }

function _defineProperty(obj, key, value) { if (key in obj) { Object.defineProperty(obj, key, { value: value, enumerable: true, configurable: true, writable: true }); } else { obj[key] = value; } return obj; }





var endpoint = "http://localhost:3000/";
var socket = socket_io_client__WEBPACK_IMPORTED_MODULE_3___default()(endpoint);

var Trello =
/*#__PURE__*/
function (_React$Component) {
  _inherits(Trello, _React$Component);

  function Trello() {
    var _getPrototypeOf2;

    var _this;

    _classCallCheck(this, Trello);

    for (var _len = arguments.length, args = new Array(_len), _key = 0; _key < _len; _key++) {
      args[_key] = arguments[_key];
    }

    _this = _possibleConstructorReturn(this, (_getPrototypeOf2 = _getPrototypeOf(Trello)).call.apply(_getPrototypeOf2, [this].concat(args)));

    _defineProperty(_assertThisInitialized(_this), "eventBus", function () {
      return undefined;
    });

    _defineProperty(_assertThisInitialized(_this), "setEventBus", function (handle) {
      _this.eventBus = handle;
    });

    _defineProperty(_assertThisInitialized(_this), "findLaneById", function (id) {
      var data = _this.state.data.lanes;
      var index = 0;
      data.map(function (val, idx) {
        if (val.id == id) {
          index = idx;
        }
      });
      return index;
    });

    _defineProperty(_assertThisInitialized(_this), "state", {
      data: {
        lanes: []
      }
    });

    _defineProperty(_assertThisInitialized(_this), "onLaneAdd", function (params) {
      var id = _this.state.data.lanes.length;
      var title = params.title;
      var newLaneData = {
        id: "lane_".concat(id),
        title: title,
        label: '0/0',
        cards: []
      };
      socket.emit("newLane", newLaneData);
    });

    _defineProperty(_assertThisInitialized(_this), "onLaneUpdate", function (params) {
      console.log(params);
    });

    _defineProperty(_assertThisInitialized(_this), "handleLaneDragEnd", function (removedIndex, addedIndex, payload) {
      var data = {
        from: removedIndex,
        to: addedIndex
      }; // socket.emit("changePosition", data)

      console.log("dari :" + removedIndex);
      console.log("ke :" + addedIndex);
      console.log(payload);
    });

    _defineProperty(_assertThisInitialized(_this), "Custom", function (props) {
      return react__WEBPACK_IMPORTED_MODULE_0___default.a.createElement("h1", null, props.title);
    });

    _defineProperty(_assertThisInitialized(_this), "onCardAdd", function (card, lineId) {
      var data = {
        card: card,
        lineId: lineId
      }; // console.log(data)

      socket.emit("newCard", data);
    });

    return _this;
  }

  _createClass(Trello, [{
    key: "componentDidMount",
    value: function componentDidMount() {
      var _this2 = this;

      var rsp = [{
        id: "lane_9120",
        title: "Halus",
        label: '0/0',
        cards: []
      }, {
        id: "lane_920",
        title: "Tes",
        label: '0/0',
        cards: []
      }];
      var data = this.state.data;
      data.lanes = rsp;
      console.log(data);
      this.setState({
        data: data
      }, function () {
        _this2.eventBus.publish({
          type: 'UPDATE_LANES',
          lanes: data.lanes
        });
      });
      socket.on("newLane", function (data) {
        var old = _this2.state.data;
        old.lanes.push(data); // console.log(data)

        _this2.eventBus.publish({
          type: 'UPDATE_LANES',
          lanes: old.lanes
        });

        _this2.setState({
          data: old
        });
      });
      socket.on("newCard", function (data) {
        var old = _this2.state.data;
        var card = data.card,
            lineId = data.lineId; // console.log(card, lineId)

        _this2.eventBus.publish({
          type: 'ADD_CARD',
          laneId: lineId,
          card: card
        });

        var idx = _this2.findLaneById(lineId);

        var currentLanes = old.lanes[idx];
        currentLanes.cards.push(card);
        old.lanes[lineId] = currentLanes;

        _this2.setState({
          data: old
        });
      });
      socket.on("changePosition", function (data) {
        var old = _this2.state.data;
        var arrOld = old.lanes;
        var from = data.from,
            to = data.to;
        var tmp = arrOld[from];
        arrOld[from] = arrOld[to];
        arrOld[to] = tmp; // console.log(data)

        _this2.eventBus.publish({
          type: 'UPDATE_LANES',
          lanes: arrOld
        });

        _this2.setState({
          data: old
        });
      });
    }
  }, {
    key: "render",
    value: function render() {
      var _this3 = this;

      return react__WEBPACK_IMPORTED_MODULE_0___default.a.createElement(react_trello__WEBPACK_IMPORTED_MODULE_2___default.a, {
        data: this.state.data,
        eventBusHandle: this.setEventBus,
        cardDragClass: "draggingCard",
        laneDragClass: "draggingLane",
        draggable: true,
        editable: true,
        onCardAdd: function onCardAdd(card, lineId) {
          return _this3.onCardAdd(card, lineId);
        },
        canAddLanes: true,
        handleLaneDragEnd: function handleLaneDragEnd(removedIndex, addedIndex, payload) {
          return _this3.handleLaneDragEnd(removedIndex, addedIndex, payload);
        },
        editLaneTitle: true,
        onLaneUpdate: function onLaneUpdate(params) {
          return _this3.onLaneUpdate(params);
        },
        onLaneAdd: function onLaneAdd(params) {
          return _this3.onLaneAdd(params);
        }
      });
    }
  }]);

  return Trello;
}(react__WEBPACK_IMPORTED_MODULE_0___default.a.Component); // export default Trello


react_dom__WEBPACK_IMPORTED_MODULE_1___default.a.render(react__WEBPACK_IMPORTED_MODULE_0___default.a.createElement(Trello, null), document.getElementById("root"));

/***/ }),

/***/ 0:
/*!********************!*\
  !*** ws (ignored) ***!
  \********************/
/*! no static exports found */
/***/ (function(module, exports) {

/* (ignored) */

/***/ })

},[["./index.js","manifest","vendor"]]]);
//# sourceMappingURL=app.js.map