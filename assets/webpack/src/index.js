import React from 'react'
import ReactDOM from 'react-dom'
import Board from 'react-trello'
import socketIOClient from "socket.io-client";
var endpoint = "http://localhost:3000/"
const socket = socketIOClient(endpoint);

class Trello extends React.Component
{
    eventBus = () => {return undefined}
    setEventBus = (handle) => {
        this.eventBus = handle
    }
    findLaneById = (id) => {
        var data = this.state.data.lanes
        var index = 0
        data.map((val, idx) => {
            if(val.id==id){
                index = idx
            }
        })
        return index
    }
    componentDidMount(){

        var rsp = [
            {
                id: "lane_9120",
                title: "Halus",
                label: '0/0',
                cards: []
            },
            {
                id: "lane_920",
                title: "Tes",
                label: '0/0',
                cards: []
            }
        ]
        var {data} = this.state
        data.lanes = rsp
        console.log(data)
        
        this.setState({data:data}, () => {
            this.eventBus.publish({type: 'UPDATE_LANES', lanes: data.lanes})
        })
        
        socket.on("newLane", data => {
            var old = this.state.data
            old.lanes.push(data)
            // console.log(data)
            this.eventBus.publish({type: 'UPDATE_LANES', lanes: old.lanes})
            this.setState({data: old})
        })
        socket.on("newCard", data => {
            var old = this.state.data
            var {card, lineId} = data
            // console.log(card, lineId)
            this.eventBus.publish({type: 'ADD_CARD', laneId: lineId, card: card})
            var idx = this.findLaneById(lineId)
            var currentLanes = old.lanes[idx]
            currentLanes.cards.push(card)
            old.lanes[lineId] = currentLanes
            this.setState({data: old})
        })
        socket.on("changePosition", data => {
            
            var old = this.state.data
            var arrOld = old.lanes
            var {from, to} = data
            var tmp = arrOld[from]
            arrOld[from] = arrOld[to]
            arrOld[to] = tmp
            // console.log(data)
            this.eventBus.publish({type: 'UPDATE_LANES', lanes: arrOld})
            this.setState({data: old})
        })
    }
    state = {
        data: {
            lanes: [
            ]
          }
    }
    onLaneAdd =(params) => {
        var id = this.state.data.lanes.length
        var {title} = params
        var newLaneData = {
            id: `lane_${id}`,
            title: title,
            label: '0/0',
            cards: []
        }
        socket.emit("newLane", newLaneData)
    }
    onLaneUpdate = params => {
        console.log(params)
    }
    handleLaneDragEnd = (removedIndex, addedIndex, payload)=> {
        var data = {from: removedIndex, to: addedIndex}
        // socket.emit("changePosition", data)
        console.log("dari :"+removedIndex)
        console.log("ke :"+addedIndex)
        console.log(payload)
    }
    Custom = (props) => {
        return(
            <h1>{props.title}</h1>
        )
    }
    onCardAdd = (card, lineId) => {
        var data = {card:card, lineId:lineId}
        // console.log(data)
        socket.emit("newCard", data)
    }
    render(){
        return(
            <Board data={this.state.data}
                
                eventBusHandle={this.setEventBus}
                cardDragClass="draggingCard"
                laneDragClass="draggingLane"
                draggable
                editable
                onCardAdd = {(card, lineId) => {return this.onCardAdd(card, lineId)}}
                canAddLanes
                handleLaneDragEnd = {(removedIndex, addedIndex, payload) => {return this.handleLaneDragEnd(removedIndex, addedIndex, payload)}}
                editLaneTitle
                onLaneUpdate={params => { return this.onLaneUpdate(params)}}
                onLaneAdd={(params) => {return this.onLaneAdd(params)}}
             />
        )
    }
}

// export default Trello
ReactDOM.render(<Trello/>, document.getElementById("root"));